	/*
 * LpGbtRegisterClerkFactory.h
 *
 *  Created on: Dec 2, 2020
 *      Author: pmoschov, pnikiel
 */

#pragma once

#include "LpGbtRegisterClerkInterface/RegisterClerkInterface.h"

namespace LpGbtSw
{

class RegisterClerkFactory
{
public:
	static RegisterClerkPtr getClerk (std::string_view address);
};

}
