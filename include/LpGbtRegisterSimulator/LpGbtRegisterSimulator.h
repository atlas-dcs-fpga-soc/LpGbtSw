#include "LpGbtRegisterClerkInterface/RegisterClerkInterface.h"

namespace LpGbtSw
{

class RegisterSimulator: public RegisterClerkInterface
{
public:
	virtual ~RegisterSimulator();

	virtual RegisterValue readRegister (RegisterAddress registerId) override;

	virtual void writeRegister (RegisterAddress registerId, RegisterValue registerValue) override;
};

}
