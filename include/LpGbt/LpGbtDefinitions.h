/*
 * LpGbtDefinitions.h
 *
 *  Created on: Dec 8, 2020
 *      Author: Paris Moschovakos
 */

#pragma once

namespace LpGbtSw
{

namespace Constants
{

    enum Specs
    {
      ADC_CHANNELS_NUM = 16,
      ADC_GAIN_SELECT_MAX = 3,
      NUMBER_WRITE_REGISTERS = 316,
      NUMBER_ALL_REGISTERS = 462
    };

}

}
