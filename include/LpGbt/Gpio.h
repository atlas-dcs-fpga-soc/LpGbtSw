/*
 * Gpio.h
 *
 *  Created on: Jan 12, 2023
 *      Author: Paris Moschovakos
 */

#pragma once

#include <iostream>

#include <LpGbtRegisterClerkInterface/RegisterClerkInterface.h>

namespace LpGbtSw
{

class Gpio
{
public:
    enum class Direction {
        input = 0,
        output = 1
    };
    enum class VoltageLevel {
        low = 0,
        high = 1
    };
    enum class DriverStrength {
        low = 0,
        high = 1
    };
    enum class Port {
        low = 0,
        high = 1
    };
    enum class PinNumber {
        // High
        PIN_0,
        PIN_1,
        PIN_2,
        PIN_3,
        PIN_4,
        PIN_5,
        PIN_6,
        PIN_7,
        // Low
        PIN_8,
        PIN_9,
        PIN_10,
        PIN_11,
        PIN_12,
        PIN_13,
        PIN_14,
        PIN_15
    };

    Gpio (RegisterClerkPtr& registerInterface);
    ~Gpio ();

private:
    RegisterClerkPtr& m_registerInterface;

private:
    // Helper functions
    template<typename RegisterValueType, typename BitPositionType, typename ValueType>
    void changeBit(RegisterValueType &registerValue, BitPositionType bitPosition, ValueType value);

    template<typename ReturnType, typename RegisterValueType, typename BitPositionType>
    ReturnType getBit(RegisterValueType registerValue, BitPositionType bitPosition);

    template<typename RegisterValueType>
    std::string registerValueToBinary(RegisterValueType registerValue);

private:
    // LpGbt Register level interface
    void setPinsDirection(Port port, RegisterValue directionMask);
    RegisterValue getPinsDirection(Port port);

    RegisterValue getInputPinsVoltageLevel(Port port);
    // The corresponding register is read only

    RegisterValue getOutputPinsVoltageLevel(Port port);
    void setOutputPinsVoltageLevel(Port port, RegisterValue voltageLevelMask);

    RegisterValue getOutputPinsSlewRate(Port port);
    void setOutputPinsSlewRate(Port port, RegisterValue driverStrengthMask);

    RegisterValue getPullInputPins(Port port);
    void setPullInputPins(Port port, RegisterValue voltageLevelMask);

public:
    // User Level interface
    void setPinDirection(PinNumber pinNumber, Direction direction);
    Direction getPinDirection(PinNumber pinNumber);

    VoltageLevel readPin(PinNumber pinNumber);

    // Only used for output pins
    void driveOutputPin(PinNumber pinNumber, VoltageLevel voltageLevel);
    void setOutputPinSlewRate(PinNumber pinNumber, DriverStrength driverStrength);

    // Only used for input pins
    void pullInputPin(PinNumber pinNumber, VoltageLevel voltageLevel);

};

}