#pragma once

#include <LpGbtRegisterClerkInterface/RegisterClerkInterface.h>
#include <LpGbtRegisterClerkFactory/RegisterClerkFactory.h>
#include <LpGbt/Adc.h>
#include <LpGbt/Gpio.h>

namespace LpGbtSw
{

class RegisterClerkFactory;

class LpGbt
{
public:
	// Construct an LpGbt by providing its address
	LpGbt(std::string_view address);

	LpGbt(RegisterClerkPtr registerClerk);

	virtual ~LpGbt() = default;
	LpGbt(const LpGbt&) = delete;
	LpGbt& operator=(const LpGbt&) = delete;
    LpGbt(LpGbt&&) = delete;
    LpGbt& operator=(LpGbt&&) = delete;

	Adc& adc() { return m_adc; }
	Gpio& gpio() { return m_gpio; }

	uint32_t readChipId();

	RegisterClerkPtr& registerClerk() { return m_registerClerk; }

private:
	RegisterClerkPtr m_registerClerk;
	Adc m_adc;
	Gpio m_gpio;

};

}
