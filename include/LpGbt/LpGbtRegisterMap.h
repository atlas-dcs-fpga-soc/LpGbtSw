/*
 * LpGbtRegisterMap.h
 *
 *  Created on: Nov 30, 2022
 *      Author: Paris Moschovakos
 *
 * Description: The file is used as a pointer to the 2 register maps of lpgbt v0 and v1
 *
 */
 
 #pragma once

#include <LpGbt/LpgbtRegisterMapV0.h>