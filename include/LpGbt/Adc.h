/*
 * Adc.h
 *
 *  Created on: Dec 8, 2020
 *      Author: Paris Moschovakos
 */

#pragma once

#include <LpGbtRegisterClerkInterface/RegisterClerkInterface.h>

namespace LpGbtSw
{

class Adc
{
public:
    enum class AdcGainSelect 
    {
        mul_2,
        mul_8,
        mul_16,
        mul_32
    };

    enum class AdcMuxControlInputs
    {
        adc0,
        adc1,
        adc2,
        adc3,
        adc4,
        adc5,
        adc6,
        adc7,
        voltageDacOutput,
        vssa,
        vddtx_mul_042,
        vddrx_mul_042,
        vdd_mul_042,
        vdda_mul_042,
        temperatureSensor,
        vref_div_2
    };

    Adc (RegisterClerkPtr& registerInterface);
    ~Adc ();

    void enable ();
    void disable ();
    void selectGain ( unsigned int gain );
  
    // Single ended measurement mode if only positive channel is given otherwise differential mode
    uint16_t convertChannel ( unsigned int inputChannel_p, 
                              unsigned int inputChannel_n = (unsigned int)AdcMuxControlInputs::vref_div_2 );

protected:
    RegisterClerkPtr& m_registerInterface;

private:
    uint8_t getAdcSelectRegister();
    uint8_t getAdcMonRegister();
    uint8_t getAdcConfigRegister();

    void selectChannel ( unsigned int inputChannel_p, 
                         unsigned int inputChannel_n );
    void convert ();
    bool isBusy ();
    bool isDone ();
    uint16_t readAdcValue();
    uint16_t pollAdc( const std::atomic<bool>& cancelled );

    void clearConvert();

    struct AdcSelect {
        unsigned int AdcInPSelect: 4;
        unsigned int AdcInNSelect: 4;
    } m_AdcSelectRegister;

    struct AdcMon {
        unsigned int TEMPSensReset: 1;
        unsigned int VDDmonEna: 1;
        unsigned int VDDTXmonEna: 1;
        unsigned int VDDRXmonEna: 1;
        unsigned int VDDANmonEna: 1;
    } m_AdcMonRegister;

    struct AdcConfig {
        unsigned int AdcConvert: 1;
        unsigned int AdcEnable: 1;
        unsigned int AdcGainSelect: 2;
    } m_AdcConfigRegister;

};

}