/*
 * LpGbtExceptions.h
 *
 *  Created on: Jan 8, 2021
 *      Author: Paris Moschovakos
 */

#pragma once

#include <stdexcept>
#include <iostream>
#include <sstream>

namespace LpGbtSw

{
   
#define LPGBTSW_EXCEPTION(aMessage) LpGbtSwException(aMessage, __FILE__, __LINE__, __PRETTY_FUNCTION__) 

inline void LpGbtSwException(std::string_view aMessage,
                        const char* fileName,
                        const std::size_t lineNumber,
                        const char* functionName)
{
   std::ostringstream stream;
   stream << "LpGbtSw exception: " << aMessage << "\n[" << fileName << ":" << lineNumber << "] in function \"" << functionName << "\"";
   throw std::runtime_error(stream.str());
}

}
