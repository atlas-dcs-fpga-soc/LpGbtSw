/*
 * RegisterClerkInterface.h
 *
 *  Created on: Dec 2, 2020
 *      Author: pmoschov, pnikiel 
 */

#pragma once

#include <memory> // shared_ptr
#include <LpGbt/LpGbtRegisterMap.h>

namespace LpGbtSw
{

	class RegisterClerkInterface;

	typedef uint8_t RegisterValue;

	typedef std::shared_ptr<RegisterClerkInterface> RegisterClerkPtr;

	class RegisterClerkInterface
	{
	public:
		virtual ~RegisterClerkInterface() {};

		//! Reads LpGbt register. Throws a subclass of std::exception.
		virtual RegisterValue readRegister (RegisterAddress registerId) = 0;

		//! Writes LpGbt register. Throws a subclass of std::exception.
		virtual void writeRegister (RegisterAddress registerId, RegisterValue registerValue) = 0;
	};

}
