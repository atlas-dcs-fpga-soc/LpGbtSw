/*
 * LpGbtUioBackendCommon.h
 *
 *  Created on: Jan 19, 2021
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 *
 *      Description : Common utility functions used by UIO backend
 */

#pragma once

#include <vector>
#include <string>

namespace LpGbtUio
{

    template< typename T >
    std::string vectorToHexString ( const std::vector<T> & v );
    // Takes value and returns the isolated bits specified by bitWidth and bitOffset. Shifts the bits to 0th position.
    uint32_t isolateBitsRead(uint32_t value, uint32_t bitWidth, uint8_t bitOffset);
    // Takes and old value and replaces the specified bits defined by value, bitWidth and bitOffset.
    uint32_t isolateBitsWrite(uint32_t value, uint32_t bitWidth, uint8_t bitOffset, uint32_t oldValue);

}