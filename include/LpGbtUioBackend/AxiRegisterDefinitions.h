#pragma once

namespace LpGbtUio
{

// Addresses:
const uint32_t magicAddr = 0x0;
const uint32_t ctrlAddr = 0x4;
const uint32_t statusAddr = 0x8;
const uint32_t dataRxAddr = 0xC;
const uint32_t dataTxAddr = 0x10;
const uint32_t registerAddr = 0x14;
const uint32_t lpgbtAddr = 0x18;
const uint32_t interruptEnableAddr = 0x1C;
const uint32_t interruptFlagsAddr = 0x20;
const uint32_t interruptClearAddr = 0x24;

// Const values:
const uint32_t lpgbtAddrValue = 0x70;
const uint32_t registerAddrValue = 0x1;
const uint32_t interruptEnableValue = 0x1;
const uint32_t interruptDisableValue = 0x0;
const uint32_t interruptClearValue = 0x1;

}