/*
 * LpGbtUioBackend.h
 *
 *  Created on: Dec 16, 2020
 *      Author: Paris Moschovakos
 */

#pragma once

#include <libuio.h>
#include <LpGbtUioBackend/AxiRegisterDefinitions.h>
#include <LpGbtUioBackend/LpGbtUioFunctions.h>
#include <LpGbtRegisterClerkInterface/RegisterClerkInterface.h>

namespace LpGbtSw
{

class LpGbtUioBackend: public RegisterClerkInterface
{
public:
	LpGbtUioBackend (std::string_view uioDeviceAddress);
	virtual ~LpGbtUioBackend();

	void initialize();
	virtual RegisterValue readRegister (RegisterAddress registerId) override;
	virtual void writeRegister (RegisterAddress registerId, RegisterValue registerValue) override;

private:
	std::string m_uioDeviceAddress;
	struct uio_info_t *m_uio;

};

}