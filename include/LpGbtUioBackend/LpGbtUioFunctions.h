/*
 * LpGbtUioFunctions.h
 *
 *  Created on: Jan 15, 2020
 *      Author: Paris Moschovakos, Morten Kristensen     
 * 
 */

#pragma once

#include <libuio.h>
#include <vector>

namespace LpGbtUio

{

uio_info_t* initUio(const std::string& uioName);
uint32_t readMagicNumber(uio_info_t* uio);
void setupUio(uio_info_t* uio);
void closeUio(uio_info_t* uio);

std::vector<uint32_t> readICRxFifo(uio_info_t* uio);
void loadRegPayload(uio_info_t* uio, uint8_t registerValue);
void writeLpgbtFifo(uio_info_t* uio);
void writeLpgbtRegisterAddress(uio_info_t* uio, uint32_t registerId);
void writeNbytesIcRxFifo(uio_info_t* uio, uint32_t value);
uint32_t readStatusRegister(uio_info_t* uio);

// Interrupts
void waitIntr(uio_info_t* uio, int waitDurationMs);
void disableIntr(uio_info_t* uio);
void enableIntr(uio_info_t* uioReg, uint32_t interruptEnableValue, uint32_t fieldWidth, uint32_t fieldOffset);
void clearIntr(uio_info_t* uio, uint32_t interruptClearValue,uint32_t fieldWidth, uint32_t fieldOffset);

}