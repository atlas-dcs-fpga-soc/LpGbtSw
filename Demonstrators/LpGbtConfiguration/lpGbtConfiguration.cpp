/**
 * LpGbt configuration tool
 *
 *  Created on: May 3, 2021
 *      Author: Paris Moschovakos
 * Description: This standalone program allows to read or configure any lpGbt with a
 * 				lpGbt configuration file as created by the official PiGBT.
 *
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <thread>
#include <chrono>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <LogIt.h>

#include <LpGbtRegisterClerkFactory/RegisterClerkFactory.h>
#include <LpGbt/LpGbt.h>
#include <LpGbt/LpGbtDefinitions.h>
#include <DemonstratorsCommon.h>

using std::vector;

// Get lpGbt configuration file name in the PiGBT format: lpgbt_config_%Y%m%d_%X.cnf
const std::string lpGbtConfigurationFilename() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "lpgbt_config_%Y%m%d_%X.cnf", &tstruct);
	std::string filename{buf};
	filename.erase(remove(filename.begin(), filename.end(), ':'), filename.end());
    return filename;
}

struct Config
{
    std::string         address;
    std::string			lpGbtConfigFile;
    std::string			data;
    int					isRead;

};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace LpGbtDemonstrators;

    Config config;
    options_description options("Options");
    std::string logLevelStr;

	options.add_options()
	("help,h", 		"show help")
	("version,v", 	"print version string")
	("address,a",   value<std::string>(&config.address)->default_value("lpgbt-uio://emp_lpgbt_4"),  helpForAddress )
	("read,r",		"read LpGbt registers")
	("write,w",		value<std::string>(&config.lpGbtConfigFile), "LpGbt configuration file path")
	;

    options_description hiddenOptions("Hidden options");
	hiddenOptions.add_options()
	("Trace level,t", 		value<std::string>(&logLevelStr)->default_value("INF"), "Trace level: ERR,WRN,INF,DBG,TRC")
	;

    options_description cmdLineOptions;
    cmdLineOptions.add(options).add(hiddenOptions);

    positional_options_description p;
	p.add("write", -1);

    variables_map vm;
    store( command_line_parser(argc, argv).options(cmdLineOptions).positional(p).run(), vm );
    notify (vm);

    if (vm.count("help"))
    {
	std::cout << options << '\n';
	exit(1);
    }
    if (vm.count("version"))
    {
    	std::cout << "1.0.0\n";
    	exit(1);
	}

    initializeLogging(logLevelStr);

    if (vm.count("read"))
	{
    	config.isRead = 1;
    }
    if (vm.count("write"))
	{
    	config.isRead = 0;
    	boost::filesystem::path p (config.lpGbtConfigFile.c_str());
		if ( !(boost::filesystem::exists(p)) ) {
			throw std::runtime_error("The configuration file doesn't exist" );
			exit(1);
		}
	}

    return config;
}

int main (int argc, char* argv[])
{
	Config config = parseProgramOptions(argc, argv);

	LpGbtSw::LpGbt lpGbt(config.address);

	try
	{
		/*
		 * LpGbt configuration write file
		 *
		 * */
		switch ( config.isRead ) {
		case 0 :
		{
			std::cout << std::setfill('-') << std::setw(120) << "-\n";
			LOG(Log::INF) << "Starting LpGbt configuration using: '" << config.lpGbtConfigFile << "'";

			std::ifstream lpGbtConfigFile(config.lpGbtConfigFile);
			std::string line;

			for (int i = 0; i <= ::LpGbtSw::Constants::Specs::NUMBER_WRITE_REGISTERS; i++)
			{

				if (lpGbtConfigFile.is_open())
					getline (lpGbtConfigFile, line);

				int rawValue = std::stoi(line.substr(3), 0, 16);

				LOG(Log::DBG) << "Writing 0x" << std::hex << rawValue << " at register " << std::dec << i;

				lpGbt.registerClerk()->writeRegister( (LpGbtSw::RegisterAddress)i, (LpGbtSw::RegisterValue)rawValue );

				std::cout << "\r" << 100*i/::LpGbtSw::Constants::Specs::NUMBER_WRITE_REGISTERS << "%" << std::flush;

			}
			lpGbtConfigFile.close();
			std::cout << std::endl;

			LOG(Log::INF) << "LpGbt Configuration Done!";
			break;
		}

		/*
		 * LpGbt read
		 *
		 * */
		case 1 :
		{

			std::cout << std::setfill('-') << std::setw(120) << "-\n";
			std::cout << "Reading LpGbt configuration... \n";

			std::string lpGbtConfigFileName = lpGbtConfigurationFilename();
			boost::filesystem::path p (lpGbtConfigFileName.c_str());
			if ( (boost::filesystem::exists(p)) ) 
			{
				throw std::runtime_error("The configuration file already exists" );
				exit(1);
			}

			std::ofstream lpGbtConfigStream(lpGbtConfigFileName);
			if(!lpGbtConfigStream)	
			{
				LOG(Log::ERR) << "Opening the LpGbt configuration file failed!";
			} 

			for (uint16_t i = 0; i <= ::LpGbtSw::Constants::Specs::NUMBER_ALL_REGISTERS; i++)
			{

				auto readBackData = lpGbt.registerClerk()->readRegister((LpGbtSw::RegisterAddress)i);

				std::cout << std::setfill(' ') << std::setw(5) << "Reg " << std::setw(3) << std::dec << i << ": "  <<
						"0x" << std::setw(2) << std::setfill('0') << std::hex << std::uppercase << (int)(readBackData) << " ";
				
				// Store only the writable registers
				if (i <= ::LpGbtSw::Constants::Specs::NUMBER_WRITE_REGISTERS)
				{
					lpGbtConfigStream << std::setw(3) << std::setfill(' ') << std::dec << i << " 0x" << std::setw(2) << 
							std::setfill('0') << std::hex << std::uppercase << (int)(readBackData) << '\n';
				}

				if ( (i+1) % 8 == 0 )
					std::cout << std::endl;
			}

			lpGbtConfigStream.close();
			std::cout << std::endl;
			std::cout << std::setfill('-') << std::setw(120) << "-\n";
			LOG(Log::INF) << "Done! LpGbt configuration saved in: " << lpGbtConfigFileName;
			break;
		}
		default:
			LOG(Log::ERR) << "Please append a file to write a LpGbt configuration";
			break;

		}
	}
	catch (const std::exception &e)
	{
		LOG(Log::ERR) << "exception " << e.what();
	}


}
