/*
 * lpGbtGpio.cpp
 *
 *  Created on: Dec 15, 2020
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 *
 *      Description : Basic Gpio demonstration
 */

#include <iostream>
#include <string>
#include <thread>
#include <chrono>

#include <boost/program_options.hpp>

#include <LogIt.h>
#include <LpGbtRegisterClerkFactory/RegisterClerkFactory.h>
#include <LpGbt/LpGbt.h>
#include <DemonstratorsCommon.h>

struct Config
{ 
    std::string     address;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace LpGbtDemonstrators;

    Config config;
    options_description options("Options");
    std::string logLevelStr;

	options.add_options()
	("help,h", 		    "show help")
	("address,a",       value<std::string>(&config.address)->default_value("lpgbt-simulator://emp_lpgbt_4"),  helpForAddress )
	;

    options_description hiddenOptions("Hidden options");
	hiddenOptions.add_options()
	("Trace level,t", value<std::string>(&logLevelStr)->default_value("TRC"), "Trace level: ERR,WRN,INF,DBG,TRC")
	;

    options_description cmdLineOptions;
    cmdLineOptions.add(options).add(hiddenOptions);

    positional_options_description p;
	p.add("write", -1);

    variables_map vm;
    store( command_line_parser(argc, argv).options(cmdLineOptions).positional(p).run(), vm );
    notify (vm);

    if (vm.count("help"))
    {
        std::cout << options << '\n';
        exit(1);
    }

    initializeLogging(logLevelStr);

    return config;
}

int main (int argc, char* argv[])
{
    
    Config config = parseProgramOptions(argc, argv);

    try
    {

        LpGbtSw::LpGbt lpGbt(config.address);

        lpGbt.gpio().setPinDirection(LpGbtSw::Gpio::PinNumber::PIN_0, LpGbtSw::Gpio::Direction::output);
        lpGbt.gpio().driveOutputPin(LpGbtSw::Gpio::PinNumber::PIN_0, LpGbtSw::Gpio::VoltageLevel::high);

        lpGbt.gpio().setPinDirection(LpGbtSw::Gpio::PinNumber::PIN_7, LpGbtSw::Gpio::Direction::input);
        auto pinValue = lpGbt.gpio().readPin(LpGbtSw::Gpio::PinNumber::PIN_7);

        LOG(Log::INF) << "The value of pin " << (int)LpGbtSw::Gpio::PinNumber::PIN_7 << " is: " << std::string((static_cast<bool>(pinValue)) ? "HIGH" : "LOW");
        
    }
    catch(const std::exception& e)
    {
        LOG(Log::ERR) << "Caught: " << e.what();
    }
    
}
