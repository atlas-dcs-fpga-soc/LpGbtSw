/*
 * DemonstratorsCommon.h
 *
 *  Created on: May 03, 2021
 *      Author: pmoschov
 * 
 */

#pragma once

#include <Common/LpGbtExceptions.h>

namespace LpGbtDemonstrators
{

using namespace LpGbtSw;
    
constexpr char helpForAddress[] =
        "LpGbt address, one of: \n"
        "lpgbt-simulator://<1>  \n"
        "lpgbt-uio://<uio_device_name> \n";

void initializeLogging (const std::string& logLevelStr)
{
    Log::LOG_LEVEL logLevel;
    if (! Log::logLevelFromString( logLevelStr, logLevel ) )
        LPGBTSW_EXCEPTION("Error when converting log level: " + logLevelStr);
    Log::initializeLogging( logLevel );
}

}