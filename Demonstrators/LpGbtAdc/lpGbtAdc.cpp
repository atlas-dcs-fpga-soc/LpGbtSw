/*
 * lpGbtAdc.cpp
 *
 *  Created on: Dec 15, 2020
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 *
 *      Description : Basic ADC conversion demonstration
 */

#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <boost/program_options.hpp>
#include <LogIt.h>

#include <LpGbtRegisterClerkFactory/RegisterClerkFactory.h>
#include <LpGbt/LpGbt.h>
#include <DemonstratorsCommon.h>

struct Config
{
    
    std::string     address;
    double          frequency;    
    unsigned int    iterations;

};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace LpGbtDemonstrators;

    Config config;
    options_description options("Options");
    std::string logLevelStr;

	options.add_options()
	("help,h", 		    "show help")
	("version,v", 	    "print version string")
	("address,a",       value<std::string>(&config.address)->default_value("lpgbt-uio://emp_lpgbt_4"),  helpForAddress )
    ("Frequency,f",	    value<double>(&config.frequency)->default_value(10), "Frequency of ADC polling (Hz)")
    ("iterations,i" ,   value<unsigned int>(&config.iterations)->default_value(std::numeric_limits<int>::max()), "Number of repetitions" )
	;

    options_description hiddenOptions("Hidden options");
	hiddenOptions.add_options()
	("Trace level,t", value<std::string>(&logLevelStr)->default_value("INF"), "Trace level: ERR,WRN,INF,DBG,TRC")
	;

    options_description cmdLineOptions;
    cmdLineOptions.add(options).add(hiddenOptions);

    positional_options_description p;
	p.add("write", -1);

    variables_map vm;
    store( command_line_parser(argc, argv).options(cmdLineOptions).positional(p).run(), vm );
    notify (vm);

    if (vm.count("help"))
    {
	std::cout << options << '\n';
	exit(1);
    }
    if (vm.count("version"))
    {
    	std::cout << "1.0.0\n";
    	exit(1);
	}

    initializeLogging(logLevelStr);

    return config;
}

int main (int argc, char* argv[])
{
    
    Config config = parseProgramOptions(argc, argv);

    try
    {

        LpGbtSw::LpGbt lpGbt(config.address);
        unsigned int i=0;

        while (i < config.iterations)
        {
            for (int inputChannel = 0; inputChannel < 8; inputChannel++)
                {
                    auto adcValue = lpGbt.adc().convertChannel( inputChannel );

                    LOG(Log::INF) << "For channel " << inputChannel << " got ADC value: " << adcValue;
                }

            std::this_thread::sleep_for(std::chrono::microseconds((long)(1000000.0 / config.frequency)));
            ++i;
        }

    }
    catch(const std::exception& e)
    {
        LOG(Log::ERR) << "Caught: " << e.what();
    }
    
}
