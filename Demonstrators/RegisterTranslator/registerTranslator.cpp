/*
 * registerTranslator.cpp
 *
 *  Created on: Dec 15, 2022
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 *
 *      Description : Register Translator example usage and performance measurement
 */

#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <boost/program_options.hpp>
#include <LogIt.h>

#include <LpGbtRegisterClerkFactory/RegisterClerkFactory.h>
#include <LpGbt/LpGbt.h>
#include <DemonstratorsCommon.h>

#define MICROBENCHMARK
#ifdef MICROBENCHMARK
	#define ANKERL_NANOBENCH_IMPLEMENT
	#include <nanobench.h>
#endif

struct Config
{
    
    std::string     address;
    double          frequency;    
    unsigned int    iterations;

};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace LpGbtDemonstrators;

    Config config;
    options_description options("Options");
    std::string logLevelStr;

	options.add_options()
	("help,h", 		    "show help")
	("version,v", 	    "print version string")
	("address,a",       value<std::string>(&config.address)->default_value("lpgbt-uio://emp_lpgbt_4"),  helpForAddress )
    ("Frequency,f",	    value<double>(&config.frequency)->default_value(10), "Frequency of ADC polling (Hz)")
    ("iterations,i" ,   value<unsigned int>(&config.iterations)->default_value(std::numeric_limits<int>::max()), "Number of repetitions" )
	;

    options_description hiddenOptions("Hidden options");
	hiddenOptions.add_options()
	("Trace level,t", value<std::string>(&logLevelStr)->default_value("INF"), "Trace level: ERR,WRN,INF,DBG,TRC")
	;

    options_description cmdLineOptions;
    cmdLineOptions.add(options).add(hiddenOptions);

    positional_options_description p;
	p.add("write", -1);

    variables_map vm;
    store( command_line_parser(argc, argv).options(cmdLineOptions).positional(p).run(), vm );
    notify (vm);

    if (vm.count("help"))
    {
	std::cout << options << '\n';
	exit(1);
    }
    if (vm.count("version"))
    {
    	std::cout << "1.0.0\n";
    	exit(1);
	}

    initializeLogging(logLevelStr);

    return config;
}

int main (int argc, char* argv[])
{
    
    Config config = parseProgramOptions(argc, argv);


    ankerl::nanobench::Bench().run("Unordered map Register Last Address to String Test", [&] {
        std::string_view reg2 = LpGbtSw::RegisterIdTranslator::toString(LpGbtSw::RegisterAddress::FASTATE);
        for(int i=1; i < 1000; i++)
        {
            reg2 = LpGbtSw::RegisterIdTranslator::toString(LpGbtSw::RegisterAddress::FASTATE);
        }
        reg2.back();
        ankerl::nanobench::doNotOptimizeAway(reg2);
    });

    ankerl::nanobench::Bench().run("Array Register Last Address to String Test", [&] {
        std::string_view reg2 = LpGbtSw::RegisterIdTranslator::toStringFast(LpGbtSw::RegisterAddress::FASTATE);
        for(int i=1; i < 1000; i++)
        {
            reg2 = LpGbtSw::RegisterIdTranslator::toStringFast(LpGbtSw::RegisterAddress::FASTATE);
        }
        reg2.back();
        ankerl::nanobench::doNotOptimizeAway(reg2);
    });

    ankerl::nanobench::Bench().run("Unordered map Register Last String to String Test", [&] {
        auto reg2 = LpGbtSw::RegisterIdTranslator::toRegisterId("FASTATE");
        for(int i=1; i < 1000; i++)
        {
            reg2 = LpGbtSw::RegisterIdTranslator::toRegisterId("FASTATE");
        }
        reg2 = LpGbtSw::RegisterAddress::ADCCAL0;
        ankerl::nanobench::doNotOptimizeAway(reg2);
    });
    
}
