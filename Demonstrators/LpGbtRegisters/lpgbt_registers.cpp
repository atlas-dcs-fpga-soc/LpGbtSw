#include <iostream>
#include <string>
#include <boost/program_options.hpp>
#include <LogIt.h>

#include <LpGbtRegisterClerkFactory/RegisterClerkFactory.h>

namespace po = boost::program_options;

void read (const std::string& address, LpGbtSw::RegisterAddress reg)
{
	std::cout << "Requested read, reg = " << LpGbtSw::RegisterIdTranslator::get().toString(reg) << '\n';
	auto clerk = LpGbtSw::RegisterClerkFactory::getClerk(address);
	auto value = clerk->readRegister(reg);
	std::cout << "Value read was: " << value << '\n';
}

int main (int argc, char* argv[])
{
	Log::initializeLogging();

	// global options

	std::string address;

	po::options_description desc("Allowed options");
	desc.add_options()
			("address,a", po::value<std::string>(&address)->default_value("sim://1"))
			("command",   po::value<std::string>(), "read or write")
			("subargs",   po::value<std::vector<std::string> >(), "Arguments for command");

	po::positional_options_description pos;
	pos.add("command", 1).
	    add("subargs", -1);

	po::variables_map vm;

	po::parsed_options parsed = po::command_line_parser(argc, argv).
	    options(desc).
	    positional(pos).
	    allow_unregistered().
	    run();

	po::store(parsed, vm);

	std::vector<std::string> remaining = po::collect_unrecognized(parsed.options, po::include_positional);
	remaining.erase(remaining.begin()); // remove 1st item - command name.
	for (auto& r : remaining)
		std::cout << r << '\n';

	std::string command = vm["command"].as<std::string>();
	if (command == "read")
	{

		po::options_description options_read("Allowed options for read are:");
		options_read.add_options()
				("register",  po::value<std::string>());
		po::positional_options_description pos;
		pos.add("register", 1);
		po::store(po::command_line_parser(remaining).options(options_read).positional(pos).run(), vm);
		std::string register_str (vm["register"].as<std::string>());
		if (register_str.substr(0, 2) != "0x")
			throw po::invalid_option_value("The register must be specified as hexadecimal ( 0x in front )");

		LpGbtSw::RegisterAddress reg = LpGbtSw::RegisterIdTranslator::toRegisterId(register_str);
		read(address, reg);

	}
	else
		throw po::invalid_option_value(command);

}
