project(LpGbtSw)
cmake_minimum_required(VERSION 3.10)

option(HAVE_UIO "Whether to build backends which require UIO" OFF)

# lots of warnings and all warnings as errors
add_compile_options(-Wall -Wextra -pedantic)

add_definitions(-std=gnu++20)

include_directories(include)

if (HAVE_UIO)
   include_directories( $ENV{UIO_HEADERS} )
   link_directories( $ENV{UIO_LIBDIRS} )
   add_definitions( -DHAVE_UIO )
endif()

# We're doing the thing below to determine if that is standalone-build (e.g. demonstrators) or included from somewhere (e.g. from OPC-UA server).
get_directory_property(hasParent PARENT_DIRECTORY)
if(hasParent)
  # intentionally empty for the moment.
else()
  message(STATUS "Seems we're building standalone LpGbtSw")
  include_directories(LogIt/include)
  set(Boost_NO_BOOST_CMAKE ON) # workaround for boost-1.7.0 cmake config modules: disabling search for boost-cmake to use FindBoost instead
  find_package(Boost REQUIRED program_options system filesystem )
  if(NOT Boost_FOUND)
    message(FATAL_ERROR "Failed to find boost installation")
  else()
	message(STATUS "Found system boost, version [${Boost_VERSION}], include dir [${Boost_INCLUDE_DIRS}] library dir [${Boost_LIBRARY_DIRS}], libs [${Boost_LIBRARIES}]")
	include_directories( ${Boost_INCLUDE_DIRS} )
	link_directories( ${Boost_LIBRARY_DIRS} )
  endif()
	add_subdirectory(LogIt)
  add_subdirectory(Demonstrators)
endif()

if (HAVE_UIO)
  file(GLOB UIO_BACKEND_SRCS
  LpGbtUioBackend/*.cpp
  )
endif()

file(GLOB SRCS
  LpGbt/*.cpp
  LpGbtRegisterClerkFactory/*.cpp
  LpGbtRegisterSimulator/*.cpp
  )

add_library(LpGbtSw OBJECT ${SRCS} ${UIO_BACKEND_SRCS} )