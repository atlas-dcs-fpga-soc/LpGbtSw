/*
 * Adc.cpp
 *
 *  Created on: Dec 8, 2020
 *      Author: Paris Moschovakos
 */

#include <chrono>
#include <thread>
#include <future>
#include <iostream>
#include <functional>
#include <atomic>

#include <LpGbt/Adc.h>
#include <LpGbt/LpGbtDefinitions.h>
#include <LpGbt/LpGbtRegisterMap.h>
#include <Common/LpGbtExceptions.h>

namespace LpGbtSw
{

Adc::Adc (RegisterClerkPtr& registerInterface): 
        m_registerInterface(registerInterface) 
{

    clearConvert();
    disable();

    enable();

    m_registerInterface->writeRegister( RegisterAddress::VREFCNTR, 0x80 );
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

}

Adc::~Adc ()
{

    clearConvert();
    disable();

}

uint8_t Adc::getAdcSelectRegister() 
{

    uint8_t registerValue = m_AdcSelectRegister.AdcInPSelect << 4 | m_AdcSelectRegister.AdcInNSelect;
    return registerValue;

}

uint8_t Adc::getAdcMonRegister()
{

    uint8_t registerValue = m_AdcMonRegister.TEMPSensReset << 5 | 
                            m_AdcMonRegister.VDDmonEna << 4 | 
                            m_AdcMonRegister.VDDTXmonEna << 3 | 
                            m_AdcMonRegister.VDDRXmonEna << 2 | 
                            m_AdcMonRegister.VDDANmonEna;
    return registerValue;

}

uint8_t Adc::getAdcConfigRegister() 
{
    
    uint8_t registerValue = m_AdcConfigRegister.AdcConvert << 7 | 
                            m_AdcConfigRegister.AdcEnable << 2 | 
                            m_AdcConfigRegister.AdcGainSelect;
    return registerValue;

}

void Adc::selectChannel ( unsigned int inputChannel_p, unsigned int inputChannel_n )
{

    if (inputChannel_p >= ::LpGbtSw::Constants::Specs::ADC_CHANNELS_NUM || inputChannel_p >= ::LpGbtSw::Constants::Specs::ADC_CHANNELS_NUM)
        throw std::out_of_range("selectChannel: argument out of range: " + std::to_string(inputChannel_p) + ", " + std::to_string(inputChannel_n) );

    m_AdcSelectRegister.AdcInPSelect = inputChannel_p;
    m_AdcSelectRegister.AdcInNSelect = inputChannel_n;

    m_registerInterface->writeRegister( RegisterAddress::ADCSELECT, getAdcSelectRegister() );

}

void Adc::enable ()
{

    m_AdcConfigRegister.AdcEnable = true;
    m_registerInterface->writeRegister( RegisterAddress::ADCCONFIG, getAdcConfigRegister());

}

void Adc::disable ()
{

    m_AdcConfigRegister.AdcEnable = false;
    m_registerInterface->writeRegister( RegisterAddress::ADCCONFIG, getAdcConfigRegister());

}

void Adc::selectGain ( unsigned int gain )
{

    if (gain > ::LpGbtSw::Constants::Specs::ADC_GAIN_SELECT_MAX)
        throw std::out_of_range("gainSelect: argument out of range: " + std::to_string(gain) );

    m_AdcConfigRegister.AdcGainSelect = gain;

    m_registerInterface->writeRegister( RegisterAddress::ADCCONFIG, getAdcConfigRegister() );

}

void Adc::convert ()
{

    m_AdcConfigRegister.AdcConvert = true;
    m_registerInterface->writeRegister( RegisterAddress::ADCCONFIG, getAdcConfigRegister());

}

bool Adc::isBusy ()
{

    uint8_t status = m_registerInterface->readRegister( RegisterAddress::ADCSTATUSH );
    bool busyBit = status & (1 << 7);
    return busyBit;

}

bool Adc::isDone ()
{

    uint8_t status = m_registerInterface->readRegister( RegisterAddress::ADCSTATUSH );
    bool doneBit = status & (1 << 6);
    return doneBit;

}

uint16_t Adc::readAdcValue()
{

    uint8_t valueHigh = m_registerInterface->readRegister( RegisterAddress::ADCSTATUSH );
    uint8_t valueLow = m_registerInterface->readRegister( RegisterAddress::ADCSTATUSL );
    uint16_t value = ( valueHigh & 0b00000011 ) << 8 | valueLow;
    return value;

}

uint16_t Adc::pollAdc( const std::atomic<bool>& cancelled )
{

    while ( !isDone() && !cancelled )
    {
        // poll every 200ms. This is most probably to be changed when timing is better understood
        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
    
    return readAdcValue();
}

// clear the convert bit to finish the conversion cycle
void Adc::clearConvert()
{

    m_AdcConfigRegister.AdcConvert = false;
    m_registerInterface->writeRegister( RegisterAddress::ADCCONFIG, getAdcConfigRegister());
    
}

uint16_t Adc::convertChannel( unsigned int inputChannel_p, unsigned int inputChannel_n )
{

    selectChannel( inputChannel_p, inputChannel_n );
    convert();

    uint16_t value;
    std::atomic<bool> cancellation_token;
    std::future<uint16_t> adcValueFuture = std::async(std::launch::async, std::bind(&Adc::pollAdc, this, std::ref(cancellation_token)));

    std::future_status status;
    do {
        status = adcValueFuture.wait_for(std::chrono::seconds(1));
        if (status == std::future_status::timeout) 
        {
            cancellation_token = true;
            value = adcValueFuture.get();
            LPGBTSW_EXCEPTION("ADC convertChannel timeout");
        } 
        else if (status == std::future_status::ready) 
        {
            value = adcValueFuture.get();
        }
    } while (status != std::future_status::ready);

    clearConvert();

    return value;

}

}
