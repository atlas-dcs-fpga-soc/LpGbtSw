#include <LpGbt/LpGbt.h>
#include <LpGbt/LpGbtRegisterMap.h>
#include <LpGbt/LpGbtDefinitions.h>

namespace LpGbtSw
{

LpGbt::LpGbt(std::string_view address):
		m_registerClerk( RegisterClerkFactory::getClerk( address ) ),
		m_adc( this->registerClerk() ),
		m_gpio( this->registerClerk() )
{

}

LpGbt::LpGbt (RegisterClerkPtr registerClerk):
		m_registerClerk(registerClerk),
		m_adc( m_registerClerk ),
		m_gpio( m_registerClerk )
{
}


uint32_t LpGbt::readChipId()
{
	uint32_t chipId =
		m_registerClerk->readRegister(RegisterAddress::CHIPID0) << 24 |
		m_registerClerk->readRegister(RegisterAddress::CHIPID1) << 16 |
		m_registerClerk->readRegister(RegisterAddress::CHIPID2) << 8 |
		m_registerClerk->readRegister(RegisterAddress::CHIPID3);
	return chipId;
}

}
