/*
 * Gpio.cpp
 *
 *  Created on: Jan 12, 2023
 *      Author: Paris Moschovakos
 */

#include <utility>
#include <bitset>

#include <LpGbt/Gpio.h>
#include <LpGbt/LpGbtRegisterMap.h>
#include <Common/LpGbtExceptions.h>
#include <LogIt.h>

using namespace LpGbtSw;

Gpio::Gpio (RegisterClerkPtr& registerInterface): 
        m_registerInterface(registerInterface) 
{}

Gpio::~Gpio() {}

template<typename RegisterValueType, typename BitPositionType, typename ValueType>
void Gpio::changeBit(RegisterValueType &registerValue, BitPositionType bitPosition, ValueType value) 
{
    std::bitset<8> registerValueBitset(registerValue);
    registerValueBitset[static_cast<int>(bitPosition)] = static_cast<bool>(value);
    registerValue = static_cast<RegisterValueType>(registerValueBitset.to_ulong());
}

template<typename ReturnType, typename RegisterValueType, typename BitPositionType>
ReturnType Gpio::getBit(RegisterValueType registerValue, BitPositionType bitPosition) 
{
    std::bitset<8> registerValueBitset(registerValue);
    return static_cast<ReturnType>(static_cast<bool>(registerValueBitset[static_cast<int>(bitPosition)]));
}

template<typename RegisterValueType>
std::string Gpio::registerValueToBinary(RegisterValueType registerValue)
{
    std::stringstream ss;
    std::bitset<8> registerValueBitset(registerValue);
    ss << registerValueBitset;
    return ss.str();
}

void Gpio::setPinsDirection(Port port, RegisterValue directionMask)
{
    switch (port) {
        break; case Gpio::Port::low:
            m_registerInterface->writeRegister( RegisterAddress::PIODIRL, directionMask );
        break; case Gpio::Port::high:
            m_registerInterface->writeRegister( RegisterAddress::PIODIRH, directionMask );
        break; default:
            LPGBTSW_EXCEPTION("Invalid GPIO port");
    }
    LOG(Log::DBG) << "Write pin direction register content: 0b" << registerValueToBinary(directionMask);
}

RegisterValue Gpio::getPinsDirection(Port port)
{
    RegisterValue pinsDirection;
    switch (port) {
        break; case Gpio::Port::low:
            pinsDirection = m_registerInterface->readRegister( RegisterAddress::PIODIRL );
        break; case Gpio::Port::high:
            pinsDirection = m_registerInterface->readRegister( RegisterAddress::PIODIRH );
        break; default:
            LPGBTSW_EXCEPTION("Invalid GPIO port");
    }
    LOG(Log::DBG) << "Read pin direction register content: 0b" << registerValueToBinary(pinsDirection);
    return pinsDirection;
}

RegisterValue Gpio::getInputPinsVoltageLevel(Port port)
{
    RegisterValue inputPinsVoltageLevel;
    switch (port) {
        break; case Gpio::Port::low:
            inputPinsVoltageLevel = m_registerInterface->readRegister( RegisterAddress::PIOINL );
        break; case Gpio::Port::high:
            inputPinsVoltageLevel = m_registerInterface->readRegister( RegisterAddress::PIOINH );
        break; default:
            LPGBTSW_EXCEPTION("Invalid GPIO port");
    }
    LOG(Log::DBG) << "Read input pin voltage level register content: 0b" << registerValueToBinary(inputPinsVoltageLevel);
    return inputPinsVoltageLevel;
}

RegisterValue Gpio::getOutputPinsVoltageLevel(Port port)
{
    RegisterValue outputPinsVoltageLevel;
    switch (port) {
        break; case Gpio::Port::low:
            outputPinsVoltageLevel = m_registerInterface->readRegister( RegisterAddress::PIOOUTL );
        break; case Gpio::Port::high:
            outputPinsVoltageLevel = m_registerInterface->readRegister( RegisterAddress::PIOOUTH );
        break; default:
            LPGBTSW_EXCEPTION("Invalid GPIO port");
    }
    LOG(Log::DBG) << "Read output pin voltage level register content: 0b" << registerValueToBinary(outputPinsVoltageLevel);
    return outputPinsVoltageLevel;
}

void Gpio::setOutputPinsVoltageLevel(Port port, RegisterValue voltageLevelMask)
{
    switch (port) {
        break; case Gpio::Port::low:
            m_registerInterface->writeRegister( RegisterAddress::PIOOUTL, voltageLevelMask );
        break; case Gpio::Port::high:
            m_registerInterface->writeRegister( RegisterAddress::PIOOUTH, voltageLevelMask );
        break; default:
            LPGBTSW_EXCEPTION("Invalid GPIO port");
    }
}

RegisterValue Gpio::getOutputPinsSlewRate(Port port)
{
    RegisterValue outputPinsSlewRate;
    switch (port) {
        break; case Gpio::Port::low:
            outputPinsSlewRate = m_registerInterface->readRegister( RegisterAddress::PIODRIVESTRENGTHL );
        break; case Gpio::Port::high:
            outputPinsSlewRate = m_registerInterface->readRegister( RegisterAddress::PIODRIVESTRENGTHH );
        break; default:
            LPGBTSW_EXCEPTION("Invalid GPIO port");
    }
    LOG(Log::DBG) << "Read output pin slew rate register content: 0b" << registerValueToBinary(outputPinsSlewRate);
    return outputPinsSlewRate;
}

void Gpio::setOutputPinsSlewRate(Port port, RegisterValue driverStrengthMask)
{
    switch (port) {
        break; case Gpio::Port::low:
            m_registerInterface->writeRegister( RegisterAddress::PIODRIVESTRENGTHL, driverStrengthMask );
        break; case Gpio::Port::high:
            m_registerInterface->writeRegister( RegisterAddress::PIODRIVESTRENGTHH, driverStrengthMask );
        break; default:
            LPGBTSW_EXCEPTION("Invalid GPIO port");
    }
}

RegisterValue Gpio::getPullInputPins(Port port)
{
    RegisterValue pullInputPins;
    switch (port) {
        break; case Gpio::Port::low:
            pullInputPins = m_registerInterface->readRegister( RegisterAddress::PIOPULLENAL );
        break; case Gpio::Port::high:
            pullInputPins = m_registerInterface->readRegister( RegisterAddress::PIOPULLENAH );
        break; default:
            LPGBTSW_EXCEPTION("Invalid GPIO port");
    }
    LOG(Log::DBG) << "Read pull input pin register content: 0b" << registerValueToBinary(pullInputPins);
    return pullInputPins;
}

void Gpio::setPullInputPins(Port port, uint8_t voltageLevelMask)
{
    switch (port) {
        break; case Gpio::Port::low:
            m_registerInterface->writeRegister( RegisterAddress::PIOPULLENAL, voltageLevelMask );
        break; case Gpio::Port::high:
            m_registerInterface->writeRegister( RegisterAddress::PIOPULLENAH, voltageLevelMask );
        break; default:
            LPGBTSW_EXCEPTION("Invalid GPIO port");
    }
}

// Users public API
void Gpio::setPinDirection(PinNumber pinNumber, Direction direction)
{
    LOG(Log::INF) << "Setting the direction of the pin " << static_cast<int>(pinNumber) << " to " << std::string((static_cast<bool>(direction)) ? "OUTPUT" : "INPUT");

    Port port;
    if ( pinNumber <= PinNumber::PIN_7 ) 
    { 
        port = Port::low;
    }
    else
    {
        port = Port::high;
        pinNumber = static_cast<PinNumber>(static_cast<int>(pinNumber) - 8);
    }
    RegisterValue registerValue = getPinsDirection(port);
    changeBit(registerValue, pinNumber, direction);
    setPinsDirection(port, registerValue);

}

Gpio::Direction Gpio::getPinDirection(PinNumber pinNumber)
{
    // get pin direction implementation
}

Gpio::VoltageLevel Gpio::readPin(PinNumber pinNumber)
{
    LOG(Log::INF) << "Reading the value of pin " << static_cast<int>(pinNumber);
    
    Port port;
    if ( pinNumber <= PinNumber::PIN_7 ) 
    { 
        port = Port::low;
    }
    else
    {
        port = Port::high;
        pinNumber = static_cast<PinNumber>(static_cast<int>(pinNumber) - 8);
    }
    RegisterValue registerValue = getInputPinsVoltageLevel(port);

    return getBit<VoltageLevel, RegisterValue, PinNumber>(registerValue, pinNumber);

}

void Gpio::driveOutputPin(PinNumber pinNumber, VoltageLevel voltageLevel)
{
    LOG(Log::INF) << "Setting the voltage level of the pin " << static_cast<int>(pinNumber) << " to " << std::string((static_cast<bool>(voltageLevel)) ? "HIGH" : "LOW");

    Port port;
    if ( pinNumber <= PinNumber::PIN_7 ) 
    { 
        port = Port::low;
    }
    else
    {
        port = Port::high;
        pinNumber = static_cast<PinNumber>(static_cast<int>(pinNumber) - 8);
    }
    RegisterValue registerValue = getOutputPinsVoltageLevel(port);
    changeBit(registerValue, pinNumber, voltageLevel);
    setOutputPinsVoltageLevel(port, registerValue);
}

void Gpio::setOutputPinSlewRate(PinNumber pinNumber, DriverStrength driverStrength)
{
    // set output pin slew rate implementation
}

void Gpio::pullInputPin(PinNumber pinNumber, VoltageLevel voltageLevel)
{
    // pull input pin implementation
}