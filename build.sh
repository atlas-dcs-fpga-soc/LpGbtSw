#!/bin/sh                                                                                                                                                                       

source ./env_settings.sh
rm -rf build
mkdir build
cd build
cmake ../ -DHAVE_UIO=1 -DCMAKE_BUILD_TYPE=Debug
make -j `nproc` || exit 1
cd ..
echo "Build standalone out-of-source"