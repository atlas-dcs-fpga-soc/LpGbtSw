# Quick startup guide

## Get the Demonstrators

For getting the latest demonstrators to check lpGBT configuration etc you can find them in the Demonstrators directory

## Build LpGbtSw

For building standalone LpGbtSw, just use

```
$ ./build.sh
```

Note that:
1. By default, the software builds with UIO for the EMP project. 
You can remove UIO dependencies by changing, in build.sh, DHAVE_UIO to 0.

2. The easiest way to get libuio is to get the repack from the EMP team from:
https://gitlab.cern.ch/atlas-dcs-fpga-soc/libuio
