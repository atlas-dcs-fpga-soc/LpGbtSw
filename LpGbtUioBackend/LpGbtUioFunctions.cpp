/*
 * LpGbtUioFunctions.cpp
 *
 *  Created on: Jan 15, 2020
 *      Author: Paris Moschovakos, Morten Kristensen     
 * 
 */

#include <cstring>
#include <iostream>
#include <stdlib.h>
#include <stdexcept>
#include <algorithm>

#include <LpGbtUioBackend/LpGbtUioFunctions.h>
#include <LpGbtUioBackend/AxiRegisterDefinitions.h>
#include <LpGbtUioBackend/LpGbtUioBackendCommon.h>
#include <Common/LpGbtExceptions.h>
#include <LogIt.h>

namespace LpGbtUio

{

using namespace LpGbtSw;

uio_info_t* initUio(const std::string& uioName)
{

    std::vector<char> uioNameC(uioName.begin(), uioName.end());
    uioNameC.push_back('\0');

    struct uio_info_t *uio;
    uio = uio_find_by_uio_name(&uioNameC[0]);

    if ( !uio )
        LPGBTSW_EXCEPTION("Could not find UIO device " + uioName);

    if( uio_open(uio) ) 
        LPGBTSW_EXCEPTION("Could not open UIO device " + uioName);

    return uio;

}

uint32_t readMagicNumber(uio_info_t* uio)
{

    uint32_t magicNumber;

    if ( uio_read32(uio, 0, magicAddr, &magicNumber) ) 
    { 
        LOG(Log::ERR) << "Failed to read magic number";
    }

    return magicNumber;

}

void setupUio(uio_info_t* uio)
{
    if ( uio_write32(uio, 0, registerAddr, registerAddrValue) )
    { 
        LOG(Log::ERR) << "Failed to write register address";
    }
    if ( uio_write32(uio, 0, lpgbtAddr, lpgbtAddrValue) )
    {
        LOG(Log::ERR) << "Failed to write lpGBT address";
    }

}

void closeUio(uio_info_t* uio)
{

    std::string uioDeviceName(uio_get_devname(uio));
    if ( uio_close(uio) ) 
    { 
        LOG(Log::ERR) << "Failed to close " + uioDeviceName; 
    }

}

void loadRegPayload(uio_info_t* uio, uint8_t registerValue)
{

    if (uio_write32(uio, 0, dataTxAddr, registerValue)) 
    { 
        LOG(Log::ERR) << "Failed to write value in AXI register"; 
    }

    if (uio_write32(uio, 0, ctrlAddr, 0x2))
    {
        LOG(Log::ERR) << "Failed to put value in tx_fifo";
    }

}

std::vector<uint32_t> readICRxFifo(uio_info_t* uio)
{

    uint32_t dataRxValue, status_v;
    std::vector<uint32_t>readValues;
    
    status_v = readStatusRegister(uio);
    status_v = isolateBitsRead(status_v, 1, 1); // Isolate to only be the empty flag
    if(status_v)
        return readValues;
    while(!status_v)
    {

        if ( uio_read32(uio, 0, dataRxAddr, &dataRxValue) )
        {
            LOG(Log::ERR) << "Failed to read data_rx";
        }
        readValues.push_back(dataRxValue);
        if ( uio_write32(uio, 0, ctrlAddr, 0x1) )
        {
            LOG(Log::ERR) << "Failed to read next byte from rx_fifo";
        }

        status_v = readStatusRegister(uio);
        status_v = isolateBitsRead(status_v, 1, 1); // Isolate to only be the empty flag

    }
    return readValues;

}

void writeLpgbtFifo(uio_info_t* uio)
{

    if ( uio_write32(uio, 0, ctrlAddr, 0x8) )
    {
        LOG(Log::ERR) << "Failed to write tx_fifo to lpGBT";
    }

}

void writeLpgbtRegisterAddress(uio_info_t* uio, uint32_t registerId)
{

    if ( uio_write32(uio, 0, registerAddr, registerId) )
    {
        LOG(Log::ERR) << "Failed to write lpGBT register address";
    }

}

void writeNbytesIcRxFifo(uio_info_t* uio, uint32_t value)
{

	if ( uio_write32(uio, 0, ctrlAddr, value) )
    {
        LOG(Log::ERR) << "Failed to set nbytes in GBT SC IC Rx FIFO ";
    }

}

uint32_t readStatusRegister(uio_info_t* uio)
{

    uint32_t statusValue;
    if ( uio_read32(uio, 0, statusAddr, &statusValue) )
    {
        LOG(Log::ERR) << "Failed to read status register";
    }
    return statusValue;

}

void waitIntr(uio_info_t* uio, int waitDurationMs)
{

    // Interrupt time
    timeval t;
    t.tv_sec = 0;
    t.tv_usec = waitDurationMs * 1000;
    std::string uioDeviceName(uio_get_devname(uio));

    if ( uio_irqwait_timeout(uio, &t) )
    {
        LPGBTSW_EXCEPTION("Failed to wait for IRQ for " + uioDeviceName);
    }

}

void enableIntr(uio_info_t* uio, uint32_t interruptEnableValue, uint32_t fieldWidth, uint32_t fieldOffset)
{

    uint32_t interruptPrevValue;
    std::string uioDeviceName(uio_get_devname(uio));

    if ( uio_enable_irq(uio) )
    {
        LOG(Log::ERR) << "Failed to enable IRQ for " << uioDeviceName;
    }

    if ( uio_read32(uio, 0, interruptEnableAddr, &interruptPrevValue) )
    {
        LOG(Log::ERR) << "Failed to read the old interrupt value";
    }
    interruptEnableValue = isolateBitsWrite(interruptEnableValue, fieldWidth, fieldOffset, interruptPrevValue);
    if ( uio_write32(uio, 0, interruptEnableAddr, interruptEnableValue) )
    {
        LOG(Log::ERR) << "Failed to set the bit corresponding to the ready interrupt";
    }

}

void clearIntr(uio_info_t* uio, uint32_t interruptClearValue, uint32_t fieldWidth, uint32_t fieldOffset)
{

    uint32_t interruptFlagsValue;

    if ( uio_read32(uio, 0, interruptFlagsAddr, &interruptFlagsValue) )
    {
        LOG(Log::ERR) << "Failed to read interrupt status";
    }
    // Clear old interrupts until everything works
    while(isolateBitsRead(interruptFlagsValue, fieldWidth, fieldOffset)) {
        if ( uio_write32(uio, 0, interruptClearAddr, interruptClearValue) )
        {
            LOG(Log::ERR) << "Failed to clear previous interrupt";
        }
        if ( uio_read32(uio, 0, interruptFlagsAddr, &interruptFlagsValue) )
        {
            LOG(Log::ERR) << "Failed to read interrupt status";
        }
    }

}

void disableIntr(uio_info_t* uio)
{

    std::string uioDeviceName(uio_get_devname(uio));
    if ( uio_write32(uio, 0, interruptEnableAddr, interruptDisableValue) )
    {
        LOG(Log::ERR) << "Failed to disable interrupts in HDL";
    }
    if ( uio_disable_irq(uio) )
    {
        LOG(Log::ERR) << "Failed to disable IRQ for " << uioDeviceName;
    }

}

}