/*
 * LpGbtUioBackendCommon.cpp
 *
 *  Created on: Jan 19, 2021
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 * 
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <LpGbtUioBackend/LpGbtUioBackendCommon.h>

namespace LpGbtUio
{

template< typename T >
std::string vectorToHexString (const std::vector<T> & v )
{

    std::stringstream ss;

    ss << std::hex << std::uppercase << std::setfill( '0' );
    for( auto value : v ) ss << std::setw( sizeof(T)*2 ) << value;

    return ss.str();

}

uint32_t isolateBitsRead(uint32_t value, uint32_t bitWidth, uint8_t bitOffset)
{

    uint32_t bitMask = (uint64_t(1) << bitWidth) -1;
    bitMask = bitMask << bitOffset;
    return ((value & bitMask) >> bitOffset);

}

uint32_t isolateBitsWrite(uint32_t value, uint32_t bitWidth, uint8_t bitOffset, uint32_t oldValue)
{

    uint32_t bitMask = (uint64_t(1) << bitWidth) -1;
    bitMask = bitMask << bitOffset;
    return((oldValue & (~bitMask)) | (value<<bitOffset));

}

template std::string vectorToHexString<uint32_t> (const std::vector<uint32_t> &);

}