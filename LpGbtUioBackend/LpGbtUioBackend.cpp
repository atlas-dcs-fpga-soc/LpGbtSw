/*
 * LpGbtUioBackend.cpp
 *
 *  Created on: Dec 16, 2020
 *      Author: Paris Moschovakos
 */

#include <LogIt.h>
#include <LpGbtUioBackend/LpGbtUioBackend.h>
#include <LpGbtUioBackend/LpGbtUioBackendCommon.h>
#include <LpGbt/LpGbtRegisterMap.h>

namespace LpGbtSw
{

LpGbtUioBackend::LpGbtUioBackend (std::string_view uioDeviceAddress):
							m_uioDeviceAddress(uioDeviceAddress),
							m_uio(LpGbtUio::initUio(m_uioDeviceAddress))
{

	LOG(Log::INF) << "Initializing uio device '" << m_uioDeviceAddress << "'";

	auto magicNumber = LpGbtUio::readMagicNumber(m_uio);
	LOG(Log::INF) << "The magic number for this uio device is: " << std::hex << magicNumber;
	
	LpGbtUio::setupUio(m_uio);
	uio_write32(m_uio, 0, LpGbtUio::registerAddr, 0x00);

}

LpGbtUioBackend::~LpGbtUioBackend()
{

	LpGbtUio::closeUio(m_uio);

}

RegisterValue LpGbtUioBackend::readRegister (RegisterAddress registerId)
{

	LOG(Log::DBG) << "Start readRegister with address " << (int)registerId;

	LpGbtUio::writeLpgbtRegisterAddress(m_uio, (uint32_t)registerId);

	LpGbtUio::clearIntr(m_uio, LpGbtUio::interruptClearValue, 0x1, 0x0);
    LpGbtUio::enableIntr(m_uio, LpGbtUio::interruptEnableValue, 0x1, 0x0);

	LpGbtUio::writeNbytesIcRxFifo(m_uio, 0x10004);

	LpGbtUio::waitIntr(m_uio, 1); // that is in ms
	LOG(Log::TRC) << "Reply received for register " << (int)registerId;
	
	// Read IC Rx FIFO up to empty
	std::vector<uint32_t>readValues = LpGbtUio::readICRxFifo(m_uio);

	LOG(Log::TRC) << "Reply payload: " << LpGbtUio::vectorToHexString<uint32_t>(readValues);

	LpGbtUio::disableIntr(m_uio);

	// TODO: The part of the reply packet that corresponds to the data, 
	// is at position 7 currently. That should be validated and possibly change
	// in the future
	return readValues[7];
	
}

void LpGbtUioBackend::writeRegister (RegisterAddress registerId, RegisterValue registerValue)
{

	LOG(Log::DBG) << "Start writeRegister with address " << (int)registerId << " and value " << (int)registerValue;

	LpGbtUio::writeLpgbtRegisterAddress(m_uio, (uint32_t)registerId);
	LpGbtUio::loadRegPayload(m_uio, registerValue);

	LpGbtUio::clearIntr(m_uio, LpGbtUio::interruptClearValue, 0x1, 0x0);
    LpGbtUio::enableIntr(m_uio, LpGbtUio::interruptEnableValue, 0x1, 0x0);

	LpGbtUio::writeLpgbtFifo(m_uio);
	LpGbtUio::waitIntr(m_uio, 1);
	LOG(Log::TRC) << "Reply received for register " << (int)registerId;

    LpGbtUio::clearIntr(m_uio, LpGbtUio::interruptClearValue, 0x1, 0x0);

	// TODO: Remove emptying FIFO when firmware provides direct
	// lpGBT register access
	std::vector<uint32_t>readValues = LpGbtUio::readICRxFifo(m_uio);

}

}