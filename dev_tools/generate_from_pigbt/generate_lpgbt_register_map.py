#!/usr/bin/env python3
#
# That script is based on lpgbt_control_lib: https://gitlab.cern.ch/lpgbt/lpgbt_control_lib
# It can be used with any of the 2 versions of lpGBT and in particular with
#
# a. lpgbt_register_map_v0.py
# b. lpgbt_register_map_v1.py
#
# Date: 30/11/2022
#
# Author: Paris Moschovakos
# Note: the original idea for this program is of Piotr Nikiel
#


import lpgbt_register_map_v0
import lpgbt_register_map_v1
from datetime import datetime

def generateLpgbtRegisterMapHeaderFile(lpgbt_rm):
    cpp = open(lpgbt_rm.__class__.__name__ + '.h', 'w')
    cpp.write(f"""/* 
 * Paris: This file was automatically generated. Please do not edit it directly
 * but rather use the template that can be found under dev_tools directory
 *
 * Generated on: {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}
 *
 */

#pragma once

#include <unordered_map>
#include <string_view>
#include <array>

namespace LpGbtSw
{{

    enum class RegisterAddress : uint16_t
    {{

""")

    for index, register in enumerate(lpgbt_rm.Reg):
        comma = ',' if index < len(lpgbt_rm.Reg)-1 else ''
        cpp.write(f'        {register.name} = 0x{register.value:03x}{comma}\n')

    cpp.write('''
    };\n
    
    template <typename T>
    concept explicitRegisterAddressType = std::same_as<T,RegisterAddress>;
    
    class RegisterIdTranslator
    {
        private:
            RegisterIdTranslator() = default;
        public:
            static RegisterIdTranslator& get(){
                static RegisterIdTranslator rit;
                return rit;
            }

            [[nodiscard("Discarding return Register string is not permitted")]]
            static std::string_view toString(const explicitRegisterAddressType auto registerAddress) noexcept {
                return get().toRegisterNameInternal(registerAddress);
            }
            [[nodiscard( "Discarding return Register string is not permitted" )]]
            static std::string_view toStringFast (const explicitRegisterAddressType auto registerAddress) noexcept {
                return get().toRegisterNameFastInternal(registerAddress); 
            }
            [[nodiscard( "Discarding return Register Address is not permitted" )]]
            static RegisterAddress toRegisterId (std::string_view registerName) noexcept { 
                return get().toRegisterAddressInternal(registerName); 
            }

            RegisterIdTranslator(const RegisterIdTranslator&) = delete;
            RegisterIdTranslator& operator=(const RegisterIdTranslator &) = delete;
            RegisterIdTranslator(RegisterIdTranslator &&) = delete;
            RegisterIdTranslator & operator=(RegisterIdTranslator &&) = delete;

        private:
            const std::unordered_map<RegisterAddress, std::string_view> RegisterAddressToName =
            {
''')

    for index, register in enumerate(lpgbt_rm.Reg):
        comma = ',' if index < len(lpgbt_rm.Reg)-1 else ''
        cpp.write(f'                {{RegisterAddress::{register.name}, "{register.name}"}}{comma}\n')

    cpp.write('''            };
            const std::unordered_map<std::string_view, RegisterAddress> RegisterNameToAddress =
            {
''')

    for index, register in enumerate(lpgbt_rm.Reg):
        comma = ',' if index < len(lpgbt_rm.Reg)-1 else ''
        cpp.write(f'                {{"{register.name}", RegisterAddress::{register.name}}}{comma}\n')

    registerList=list(lpgbt_rm.Reg)
    cpp.write(f"""            }};
            const std::array<std::string_view, {registerList[-1].value + 1}> registerAssociativeArray =
            {{
""")
    for index, register in enumerate(lpgbt_rm.Reg):
        comma = ',' if index < len(lpgbt_rm.Reg)-1 else ''
        cpp.write(f'                "{register.name}"{comma}\n')

    cpp.write('''            };\n
        public:
            std::string_view toRegisterNameInternal (auto registerAddress) const noexcept {
                return RegisterAddressToName.at(registerAddress); 
            }
            std::string_view toRegisterNameFastInternal (auto registerAddress) const noexcept {
                return registerAssociativeArray[std::size_t(registerAddress)]; 
            }
            RegisterAddress toRegisterAddressInternal (std::string_view registerName) const noexcept { 
                return RegisterNameToAddress.at(registerName); 
            }
        
    };

}\n''')

def main():
    generateLpgbtRegisterMapHeaderFile(lpgbt_register_map_v0.LpgbtRegisterMapV0())
    generateLpgbtRegisterMapHeaderFile(lpgbt_register_map_v1.LpgbtRegisterMapV1())
    
if __name__ == "__main__":
    main()