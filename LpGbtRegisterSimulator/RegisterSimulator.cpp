#include <LpGbtRegisterSimulator/LpGbtRegisterSimulator.h>
#include <LpGbt/LpGbtRegisterMap.h>

#include <LogIt.h>

namespace LpGbtSw
{

RegisterSimulator::~RegisterSimulator()
{

}

RegisterValue RegisterSimulator::readRegister (RegisterAddress registerId)
{
	switch (registerId)
	{
		case RegisterAddress::CHIPID0: return 0xde;
		case RegisterAddress::CHIPID1: return 0xad;
		case RegisterAddress::CHIPID2: return 0xbe;
		case RegisterAddress::CHIPID3: return 0xef;
		case RegisterAddress::ADCSTATUSH: return rand();
		case RegisterAddress::ADCSTATUSL: return rand();
		case RegisterAddress::PIODIRL: return 0x00;
		case RegisterAddress::PIODIRH: return 0x00;
		case RegisterAddress::PIOINL: return 0x00;
		case RegisterAddress::PIOINH: return 0x00;
		case RegisterAddress::PIOOUTL: return 0x00;
		case RegisterAddress::PIOOUTH: return 0x00;
		default:
			LOG(Log::WRN) << "Simulator is returning a noise for register that is not yet simulated!! (register: " << RegisterIdTranslator::toString(registerId) << ")";
			return rand();
	}
}

void RegisterSimulator::writeRegister (RegisterAddress registerId, RegisterValue registerValue)
{
	switch (registerId)
	{
		break; case RegisterAddress::ADCSELECT:
		break; case RegisterAddress::ADCCONFIG: 
		break; case RegisterAddress::VREFCNTR:
		break; case RegisterAddress::PIODIRL:
		break; case RegisterAddress::PIODIRH:
		break; case RegisterAddress::PIOINL:
		break; case RegisterAddress::PIOINH:
		break; case RegisterAddress::PIOOUTL:
		break; case RegisterAddress::PIOOUTH:
		break; default: 
		{
			LOG(Log::ERR) << "simulated write register not implemented, register:" << RegisterIdTranslator::toString(registerId) << " value " + std::to_string(registerValue);
			throw std::runtime_error("Not implemented");
		}
	}
}

}
