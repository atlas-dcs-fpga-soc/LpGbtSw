/*
 * LpGbtRegisterClerkFactory.cpp
 *
 *  Created on: Dec 2, 2020
 *      Author: pnikiel, pmoschov
 */

#include <LogIt.h>

#include <LpGbtRegisterClerkFactory/RegisterClerkFactory.h>
#include <LpGbtRegisterSimulator/LpGbtRegisterSimulator.h>
#include <Common/LpGbtExceptions.h>

#ifdef HAVE_UIO
#include <LpGbtUioBackend/LpGbtUioBackend.h>
#endif

using namespace std::string_literals;

namespace LpGbtSw

{

RegisterClerkPtr RegisterClerkFactory::getClerk (std::string_view lpGbtBackendAddress)
{

	LOG(Log::INF) << "getClerk for " << lpGbtBackendAddress;

	std::string separator ("://");

	auto n = lpGbtBackendAddress.find( separator );
	if (n == std::string::npos)
		LPGBTSW_EXCEPTION("Could not find a separator in the lpGbt address: ");

	auto lpgbtBackendType = lpGbtBackendAddress.substr(0, n);
	auto specificAddress = lpGbtBackendAddress.substr(n + separator.length());

	LOG(Log::DBG)  << "backendType: '" << lpgbtBackendType << "' specificAddress: '" << specificAddress << "'";

	RegisterClerkPtr backend = nullptr;

	if (lpgbtBackendType == "lpgbt-simulator")
	{
		backend = std::make_shared<RegisterSimulator>();
	}
	else
	#ifdef HAVE_UIO
	if (lpgbtBackendType == "lpgbt-uio")
		backend = std::make_shared<LpGbtUioBackend>(specificAddress);
	else
	#endif
	{
		LPGBTSW_EXCEPTION("The chosen backend is illegal: ");
	}

	return backend;

}

}
