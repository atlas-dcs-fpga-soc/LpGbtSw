# This script must be sourced and NOT executed. 
# For instance do:
# 
#   source setup_paths.sh
#
# Authors: Paris

### To move to a different LCG edit the following variables accordingly
LCG_VERSION="LCG_102"
PLATFORM="centos7"
COMPILER="gcc11"
GCC_VERSION="11.3.0"
CMAKE_VERSION="3.20.0"
BOOST_VERSION="1.78.0"
###

isP1() {
    IP_FIELD1=`hostname -I | cut -d '.' -f 1`
    if [ "$IP_FIELD1" = "10" ]; then
        echo "Info: We are in P1"
        return 0
    else
		echo "Info: We are in GPN"
        return 1
    fi
}

if isP1; then
	PATH_TO_LCG="/sw/atlas/sw/lcg"
else
	PATH_TO_LCG="/cvmfs/sft.cern.ch/lcg"
fi

X_BINARY_TAG_SHORT="x86_64-${PLATFORM}"
X_BINARY_TAG="${X_BINARY_TAG_SHORT}-${COMPILER}-opt"

usage()
{
	echo
	echo "Usage"
	echo
	echo "[-h]: help"
	echo
}

epilogue()
{
	printf '%21s %s \n' \
		"LCG:" "[${PATH_TO_LCG}/releases/${LCG_VERSION}]" \
		"c++ (GCC):" "[${PATH_TO_LCG}/releases/gcc/${GCC_VERSION}/${X_BINARY_TAG}]" \
		"CMake:" "[${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/${CMAKE_VERSION}/${X_BINARY_TAG}]" \
		"BOOST_ROOT:" "[${BOOST_ROOT}]"
}
	
grep -e "CentOS" /etc/redhat-release &> /dev/null
rc=$?
if [ $rc == 0 ]
then
	if [ "$1" == "-h" ]
	then
		usage
	else

		#  First the compiler
		if [ -e ${PATH_TO_LCG}/releases/gcc/${GCC_VERSION}/${X_BINARY_TAG_SHORT}/setup.sh ]; then
    		source ${PATH_TO_LCG}/releases/gcc/${GCC_VERSION}/${X_BINARY_TAG_SHORT}/setup.sh
		else
			echo "Error: Cannot access ${PATH_TO_LCG}/releases/gcc/${GCC_VERSION}/${X_BINARY_TAG_SHORT}/setup.sh"
		 	exit 1
		fi
		#  Then CMake
		if [ -e ${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/${CMAKE_VERSION}/${X_BINARY_TAG}/CMake-env.sh ]; then
			#source ${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/${CMAKE_VERSION}/${X_BINARY_TAG}/CMake-env.sh
			export PATH=${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/${CMAKE_VERSION}/${X_BINARY_TAG}/bin:$PATH
		else
			echo "Error: Cannot access ${PATH_TO_LCG}/releases/${LCG_VERSION}/CMake/${CMAKE_VERSION}/${X_BINARY_TAG}/CMake-env.sh"
		 	exit 1
		fi
		#  Then Boost
		export BOOST_ROOT="${PATH_TO_LCG}/releases/${LCG_VERSION}/Boost/${BOOST_VERSION}/${X_BINARY_TAG}"
		epilogue
	fi
fi
